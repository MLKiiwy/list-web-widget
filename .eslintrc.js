module.exports = {
  extends: 'airbnb',
  parser: 'babel-eslint',
  env: {
    browser: true,
    'jest/globals': true,
    node: true
  },
  plugins: [
    'jest',
    'react-hooks'
  ],
  rules: {
    'arrow-parens': [2, 'as-needed'],
    'consistent-return': 0,
    'import/no-extraneous-dependencies': 0,
    'import/no-named-as-default': 0,
    'import/order': [2, {
      groups: [
        ['builtin', 'external'],
        'sibling',
        'parent',
        'index'
      ],
      'newlines-between': 'always'
    }],
    'import/prefer-default-export': 0,
    "no-multiple-empty-lines": [2, {
      "max": 1
    }],
    'no-console': [2, {
      allow: ['error', 'info']
    }],
    'no-param-reassign': [2, {
      props: false
    }],
    'no-plusplus': 0,
    'no-shadow': 0,
    'no-unused-expressions': 0,
    'no-nested-ternary': 0,
    'object-curly-newline': [2, {
      multiline: true,
      consistent: true
    }],
    'react/jsx-filename-extension': [2, {
      extensions: ['.js']
    }],
    'react/jsx-sort-default-props': 2,
    'react/sort-prop-types': 2,
    'react-hooks/rules-of-hooks': 2
  }
};
