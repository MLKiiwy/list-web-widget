import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs';

import Popper, { PLACEMENT } from '..';

import RoundButton from '../../RoundButton';

storiesOf('Popper', module)
  .addDecorator(withKnobs)
  .add('Placement bottom', () => (
    <div style={{ display: 'flex', paddingTop: '50px', justifyContent: 'center' }}>
      <RoundButton id="anchor" />
      <Popper open={boolean('Open', false)} placement={PLACEMENT.BOTTOM} anchorEl={document.getElementById('anchor')}>
        Content
      </Popper>
    </div>
  ))
  .add('Placement top', () => (
    <div style={{ display: 'flex', paddingTop: '50px', justifyContent: 'center' }}>
      <RoundButton id="anchor" />
      <Popper open={boolean('Open', false)} placement={PLACEMENT.TOP} anchorEl={document.getElementById('anchor')}>
        Content
      </Popper>
    </div>
  ));
