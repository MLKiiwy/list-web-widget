import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PopperMaterial from '@material-ui/core/Popper/Popper';
import Paper from '@material-ui/core/Paper';

export const PLACEMENT = {
  TOP: 'top',
  BOTTOM: 'bottom',
};

const styles = () => ({
  popper: {
    zIndex: 1,
    margin: '0.3em 0 0.3em 0',
    '&[x-placement*="bottom"] $arrow': {
      top: 0,
      left: 0,
      marginTop: '-0.9em',
      width: '3em',
      height: '1em',
      '&::before': {
        borderWidth: '0 1em 1em 1em',
        borderColor: 'transparent transparent white transparent',
      },
    },
    '&[x-placement*="top"] $arrow': {
      bottom: 0,
      left: 0,
      marginBottom: '-0.9em',
      width: '3em',
      height: '1em',
      '&::before': {
        borderWidth: '1em 1em 0 1em',
        borderColor: 'white transparent transparent transparent',
      },
    },
    '&[x-placement*="right"] $arrow': {
      left: 0,
      marginLeft: '-0.9em',
      height: '3em',
      width: '1em',
      '&::before': {
        borderWidth: '1em 1em 1em 0',
        borderColor: 'transparent white transparent transparent',
      },
    },
    '&[x-placement*="left"] $arrow': {
      right: 0,
      marginRight: '-0.9em',
      height: '3em',
      width: '1em',
      '&::before': {
        borderWidth: '1em 0 1em 1em',
        borderColor: 'transparent transparent transparent white',
      },
    },
  },
  arrow: {
    position: 'absolute',
    fontSize: 10,
    width: '4em',
    height: '4em',
    '&::before': {
      content: '""',
      margin: 'auto',
      display: 'block',
      width: 0,
      height: 0,
      borderStyle: 'solid',
    },
  },
  paper: {
    padding: '10px',
    maxWidth: '450px',
  },
});

class Popper extends PureComponent {
  state = {
    arrowRef: null,
  };

  handleArrowRef = node => {
    this.setState({
      arrowRef: node,
    });
  };

  render() {
    const {
      classes, open, anchorEl, children, onClose, placement,
    } = this.props;
    const { arrowRef } = this.state;
    return (
      <PopperMaterial
        className={classes.popper}
        open={open}
        anchorEl={anchorEl}
        placement={placement}
        modifiers={{
          flip: {
            enabled: false,
          },
          preventOverflow: {
            enabled: true,
            boundariesElement: 'window',
          },
          arrow: {
            enabled: true,
            element: arrowRef,
          },
        }}
        onClose={onClose}
        x-placement={placement === PLACEMENT.TOP ? PLACEMENT.BOTTOM : PLACEMENT.TOP}
      >
        <span className={classes.arrow} ref={this.handleArrowRef} />
        <Paper className={classes.paper}>{children}</Paper>
      </PopperMaterial>
    );
  }
}

Popper.propTypes = {
  anchorEl: PropTypes.oneOfType([PropTypes.node, PropTypes.element]),
  children: PropTypes.node.isRequired,
  // eslint-disable-next-line
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  placement: PropTypes.oneOf([PLACEMENT.TOP, PLACEMENT.BOTTOM]).isRequired,
};

Popper.defaultProps = {
  anchorEl: undefined,
  onClose: () => {},
};

export default withStyles(styles)(Popper);
