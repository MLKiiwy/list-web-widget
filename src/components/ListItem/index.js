import React from 'react';
import PropTypes from 'prop-types';
import ListItemMaterialUi from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import HelpIcon from '@material-ui/icons/HelpOutline';
import ErrorIcon from '@material-ui/icons/ErrorOutline';
import { withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import HashmapPropType from 'hashmap-prop-type';
import blue from '@material-ui/core/colors/blue';

import RoundButton from '../RoundButton';

const styles = () => ({
  root: {},
});

function ListItem({ children, loading, error, classes, className, item, colorBase }) {
  const classNameValue = clsx(classes.root, className);
  const itemIcon = error ? <ErrorIcon /> : <HelpIcon />;
  let { name, description: subtitle } = item;
  if (loading) {
    name = item.title || 'Loading ...';
    subtitle = item.description;
  } else if (error) {
    name = 'Error';
    subtitle = error.message;
  }
  return (
    <ListItemMaterialUi key={item.id} dense className={classNameValue}>
      <RoundButton loading={loading} icon={itemIcon} colorBase={colorBase} />
      <ListItemText primary={name} secondary={subtitle} />
      {children}
    </ListItemMaterialUi>
  );
}

ListItem.propTypes = {
  children: PropTypes.node,
  // eslint-disable-next-line
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  colorBase: HashmapPropType(PropTypes.string.isRequired),
  error: PropTypes.shape({
    message: PropTypes.string.isRequired,
    onRetry: PropTypes.func,
  }),
  item: PropTypes.shape({
    description: PropTypes.string,
    id: PropTypes.string.isRequired,
    name: PropTypes.string,
  }),
  loading: PropTypes.bool,
};

ListItem.defaultProps = {
  children: undefined,
  className: undefined,
  colorBase: blue,
  error: undefined,
  item: undefined,
  loading: false,
};

export default withStyles(styles)(ListItem);
