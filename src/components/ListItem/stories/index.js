import React from 'react';
import { storiesOf } from '@storybook/react';
import Paper from '@material-ui/core/Paper/Paper';

import ListView from '../../ListView';

import ListItem from '..';

storiesOf('ListItem', module)
  .addDecorator(story => <Paper>{story()}</Paper>)
  .add('Loading', () => (
    <ListView items={[{ id: '1', name: 'Item name' }]}>
      <ListItem loading />
    </ListView>
  ))
  .add('Error', () => (
    <ListView items={[{ id: '1', name: 'Item name' }]}>
      <ListItem error={{ message: 'Error message content' }} />
    </ListView>
  ))
  .add('Not fully loaded item', () => (
    <ListView items={[{ id: '1', name: 'Item name' }]}>
      <ListItem loading />
    </ListView>
  ))
  .add('Fully loaded item', () => (
    <ListView items={[{ id: '1', name: 'Item name', description: 'Item description content' }]}>
      <ListItem />
    </ListView>
  ));
