import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import ErrorLabel from '..';

storiesOf('ErrorLabel', module)
  .add('Default', () => <ErrorLabel>An error message</ErrorLabel>)
  .add('With retry', () => <ErrorLabel onRetry={action('on retry')}>An error message</ErrorLabel>);
