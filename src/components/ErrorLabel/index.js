import React from 'react';
import PropTypes from 'prop-types';
import ErrorIcon from '@material-ui/icons/Error';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    flexGrow: 1,
    overflow: 'hidden',
    flexDirection: 'row',
    textAlign: 'center',
  },
  button: {
    margin: theme.spacing.unit,
  },
});

function ErrorLabel({ children, onRetry, classes, className }) {
  const classNameValue = clsx(classes.root, className);
  return (
    <div className={classNameValue}>
      <ErrorIcon />
      <div>
        <span>{children}</span>
      </div>
      {onRetry && (
        <Button variant="contained" size="small" onClick={onRetry} className={classes.button}>
          Retry
        </Button>
      )}
    </div>
  );
}

ErrorLabel.propTypes = {
  children: PropTypes.string.isRequired,
  // eslint-disable-next-line
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  onRetry: PropTypes.bool.isRequired,
};

ErrorLabel.defaultProps = {
  className: undefined,
};

export default withStyles(styles)(ErrorLabel);
