import React, { Component } from 'react';

const calculateViewport = () => ({
  width: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
  height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
});

function withViewport(ComposedComponent) {
  return class WithViewport extends Component {
    constructor(props) {
      super(props);

      this.handleResize = this.handleResize.bind(this);

      this.state = calculateViewport();
    }

    componentDidMount() {
      window.addEventListener('resize', this.handleResize);
      window.addEventListener('orientationchange', this.handleResize);
    }

    componentWillUnmount() {
      window.removeEventListener('resize', this.handleResize);
      window.removeEventListener('orientationchange', this.handleResize);
    }

    handleResize() {
      const { width, height } = this.state;
      const newValues = calculateViewport();
      if (width !== newValues.width || height !== newValues.height) {
        this.setState(newValues);
      }
    }

    render() {
      return <ComposedComponent {...this.props} viewport={this.state} />;
    }
  };
}

export default withViewport;
