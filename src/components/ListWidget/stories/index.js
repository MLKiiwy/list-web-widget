import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import apolloStorybookDecorator from 'apollo-storybook-react';
import { typeDefs } from 'list-graphql';

import imageFile from '../../../../static/images/listHead_test.jpg';

import ListWidget from '..';

const lists = {
  'list-1': {
    id: 'list-1',
    items: [
      { id: 'list-1-item-1', name: 'Item 1', description: 'Item description' },
      { id: 'list-1-item-2', name: 'Item 2', description: 'Item description' },
    ],
  },
  'list-2': {
    id: 'list-2',
    items: [
      { id: 'list-2-item-1', name: 'Item 1', description: 'Item description' },
      { id: 'list-2-item-2', name: 'Item 2', description: 'Item description' },
      { id: 'list-2-item-3', name: 'Item 3', description: 'Item description' },
      { id: 'list-2-item-4', name: 'Item 4', description: 'Item description' },
    ],
  },
};

const mocks = {
  Query: () => ({
    lists: () => [{ id: 'list-1', image: '' }, { id: 'list-2', image: imageFile }],
    list: (root, params) => lists[params.id],
  }),
};

storiesOf('ListWidget', module)
  .addDecorator(withKnobs)
  .addDecorator(
    apolloStorybookDecorator({
      typeDefs,
      mocks,
    }),
  )
  .add('Default', () => <ListWidget />);
