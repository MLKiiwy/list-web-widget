import React, { Component, Fragment } from 'react';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import DragableListButton, { POSITIONS } from '../DragableListButton';
import ListViewContainer from '../../containers/ListViewContainer';
import Popper, { PLACEMENT } from '../Popper';

class ListWidget extends Component {
  constructor(props) {
    super(props);

    this.state = {
      openedListId: undefined,
      popoverAnchorElement: undefined,
      buttonPosition: POSITIONS.BOTTOM_RIGHT,
    };
  }

  handleOpenList = (listId, event) => {
    const { openedListId } = this.state;
    if (openedListId) {
      this.setState({ openedListId: undefined });
    } else {
      this.setState({ openedListId: listId, popoverAnchorElement: event.currentTarget });
    }
  };

  handlePopoverClose = () => {
    this.setState({ openedListId: undefined });
  };

  handlePositionChange = buttonPosition => {
    this.setState({
      buttonPosition,
    });
  };

  handleClickAway = () => {
    this.setState({ openedListId: undefined });
  };

  render() {
    const { openedListId, popoverAnchorElement, buttonPosition } = this.state;
    const popoverOpen = openedListId !== undefined;
    return (
      <Fragment>
        <DragableListButton
          onOpenList={this.handleOpenList}
          onPositionChange={this.handlePositionChange}
        />
        <Popper
          open={popoverOpen}
          anchorEl={popoverAnchorElement}
          onClose={this.handlePopoverClose}
          placement={
            buttonPosition === POSITIONS.TOP_RIGHT || buttonPosition === POSITIONS.TOP_LEFT
              ? PLACEMENT.BOTTOM
              : PLACEMENT.TOP
          }
        >
          <ClickAwayListener onClickAway={this.handleClickAway}>
            <ListViewContainer id={openedListId} />
          </ClickAwayListener>
        </Popper>
      </Fragment>
    );
  }
}

export default ListWidget;
