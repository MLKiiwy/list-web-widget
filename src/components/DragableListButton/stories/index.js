import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import DraggableListButton, { POSITIONS } from '..';

import imageFile from '../../../../static/images/listHead_test.jpg';
import ListHeadButtonList from '../../ListHeadButtonList';

const lists = [
  {
    id: '1',
    image: imageFile,
  },
  {
    id: '2',
  },
  {
    id: '3',
  },
];

storiesOf('DraggableListButton', module)
  .addDecorator(withKnobs)
  .add('Default, bottom left', () => (
    <DraggableListButton onOpenList={action('On open list')}>
      <ListHeadButtonList lists={lists} />
    </DraggableListButton>
  ))
  .add('Bottom left', () => (
    <DraggableListButton startPosition={POSITIONS.BOTTOM_LEFT} onOpenList={action('On open list')}>
      <ListHeadButtonList lists={lists} />
    </DraggableListButton>
  ))
  .add('Top left', () => (
    <DraggableListButton startPosition={POSITIONS.TOP_LEFT} onOpenList={action('On open list')}>
      <ListHeadButtonList lists={lists} />
    </DraggableListButton>
  ))
  .add('Top right', () => (
    <DraggableListButton startPosition={POSITIONS.TOP_RIGHT} onOpenList={action('On open list')}>
      <ListHeadButtonList lists={lists} />
    </DraggableListButton>
  ));
