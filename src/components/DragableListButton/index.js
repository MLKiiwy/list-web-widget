import Draggable from 'react-draggable';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

import withViewport from '../withViewport';
import { ORIENTATION_TO_RIGHT, ORIENTATION_TO_LEFT } from '../ListHeadButtonList';
import ListHeadButtonListContainer from '../../containers/ListHeadButtonListContainer';
import { DEFAULT_ROUND_SIZE, PADDING_VIEWPORT_WIDGET } from '../../constants';

export const POSITIONS = {
  TOP_LEFT: 'TOP_LEFT',
  TOP_RIGHT: 'TOP_RIGHT',
  BOTTOM_LEFT: 'BOTTOM_LEFT',
  BOTTOM_RIGHT: 'BOTTOM_RIGHT',
};

const styles = () => ({
  paper: {
    top: DEFAULT_ROUND_SIZE,
  },
  viewport: {
    position: 'fixed',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});

class DraggableListButton extends Component {
  constructor(props) {
    super(props);
    const { startPosition } = this.props;

    this.state = {
      open: false,
      position: startPosition,
      dragging: false,
    };
  }

  getBounds = () => {
    const { viewport } = this.props;

    return {
      left: PADDING_VIEWPORT_WIDGET,
      top: PADDING_VIEWPORT_WIDGET,
      right: viewport.width - (DEFAULT_ROUND_SIZE + PADDING_VIEWPORT_WIDGET),
      bottom: viewport.height - (DEFAULT_ROUND_SIZE + PADDING_VIEWPORT_WIDGET),
    };
  };

  handleOpenList = (id, event) => {
    const { dragging } = this.state;
    const { onOpenList } = this.props;

    if (dragging) {
      return;
    }
    event.stopPropagation();
    onOpenList(id, event);
  };

  handleOpen = event => {
    const { dragging } = this.state;

    if (dragging) {
      return;
    }
    event.stopPropagation();
    this.setState({ open: true });
  };

  handleClose = event => {
    const { dragging } = this.state;
    if (dragging) {
      return;
    }
    event.stopPropagation();
    this.setState({ open: false });
  };

  handleStop = event => {
    const { dragging, position } = this.state;
    const { onPositionChange } = this.props;
    event.stopPropagation();
    if (dragging) {
      const { viewport } = this.props;
      let newPosition;
      if (event.x >= viewport.width / 2) {
        newPosition = event.y >= viewport.height / 2 ? POSITIONS.BOTTOM_RIGHT : POSITIONS.TOP_RIGHT;
      } else {
        newPosition = event.y >= viewport.height / 2 ? POSITIONS.BOTTOM_LEFT : POSITIONS.TOP_LEFT;
      }
      if (newPosition !== position) {
        onPositionChange(newPosition);
      }
      this.setState({ position: newPosition, dragging: false });
    }
  };

  handleDrag = () => {
    const { dragging } = this.state;

    if (!dragging) {
      this.setState({ dragging: true });
    }
  };

  calculateOrientation = () => {
    const { dragging } = this.state;

    if (dragging) {
      return ORIENTATION_TO_RIGHT;
    }
    const { position } = this.state;
    switch (position) {
      default:
      case POSITIONS.BOTTOM_RIGHT:
      case POSITIONS.TOP_RIGHT:
        return ORIENTATION_TO_LEFT;
      case POSITIONS.TOP_LEFT:
      case POSITIONS.BOTTOM_LEFT:
        return ORIENTATION_TO_RIGHT;
    }
  };

  calculateCoordinates = () => {
    const { position } = this.state;
    const bounds = this.getBounds();
    switch (position) {
      default:
      case POSITIONS.BOTTOM_RIGHT:
        return { x: bounds.right + DEFAULT_ROUND_SIZE, y: bounds.bottom };
      case POSITIONS.BOTTOM_LEFT:
        return { x: bounds.left, y: bounds.bottom };
      case POSITIONS.TOP_LEFT:
        return { x: bounds.left, y: bounds.top };
      case POSITIONS.TOP_RIGHT:
        return { x: bounds.right + DEFAULT_ROUND_SIZE, y: bounds.top };
    }
  };

  renderChildren = () => {
    const { children } = this.props;
    const { open } = this.state;
    return React.cloneElement(children, {
      open,
      orientation: this.calculateOrientation(),
      onOpen: this.handleOpen,
      onOpenList: this.handleOpenList,
      onClose: this.handleClose,
    });
  }

  render() {
    const { classes } = this.props;
    const { open } = this.state;
    const bounds = this.getBounds();
    const coordinates = this.calculateCoordinates();
    return (
      <div className={classes.viewport}>
        <Draggable
          disabled={open}
          bounds={bounds}
          position={coordinates}
          onDrag={this.handleDrag}
          onStop={this.handleStop}
        >
          <div>
            {this.renderChildren()}
          </div>
        </Draggable>
      </div>
    );
  }
}

DraggableListButton.propTypes = {
  children: PropTypes.node,
  // eslint-disable-next-line
  classes: PropTypes.object.isRequired,
  onOpenList: PropTypes.func.isRequired,
  onPositionChange: PropTypes.func,
  startPosition: PropTypes.oneOf([
    POSITIONS.TOP_LEFT,
    POSITIONS.TOP_RIGHT,
    POSITIONS.BOTTOM_LEFT,
    POSITIONS.BOTTOM_RIGHT,
  ]),
  viewport: PropTypes.shape({
    height: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
  }).isRequired,
};

DraggableListButton.defaultProps = {
  children: <ListHeadButtonListContainer />,
  onPositionChange: () => {},
  startPosition: POSITIONS.BOTTOM_RIGHT,
};

export default withStyles(styles)(withViewport(DraggableListButton));
