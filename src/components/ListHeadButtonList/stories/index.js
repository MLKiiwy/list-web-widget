import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import ListHeadButtonList, { ORIENTATION_TO_RIGHT, ORIENTATION_TO_LEFT } from '..';

import imageFile from '../../../../static/images/listHead_test.jpg';

const lists = [
  {
    id: '1',
    image: imageFile,
  },
  {
    id: '2',
  },
  {
    id: '3',
  },
];

storiesOf('ListHeadButtonList', module)
  .addDecorator(withKnobs)
  .addDecorator(story => (
    <div
      style={{
        position: 'absolute',
        width: '90%',
        height: '90%',
        left: '5%',
        top: '5%',
        border: '2px dashed black',
      }}
    >
      {story()}
    </div>
  ))
  .add('Default', () => (
    <ListHeadButtonList lists={lists} onOpen={action('onOpen')} onOpenList={action('onOpenList')} onClose={action('onClose')} />
  ))
  .add('Loading', () => (
    <ListHeadButtonList
      lists={lists}
      loading
      onOpen={action('onOpen')}
      onOpenList={action('onOpenList')}
      onClose={action('onClose')}
    />
  ))
  .add('Orientation: toRight', () => (
    <div style={{ position: 'absolute', left: 0, top: 0 }}>
      <ListHeadButtonList
        open={boolean('open', true)}
        lists={lists}
        orientation={ORIENTATION_TO_RIGHT}
        onOpen={action('onOpen')}
        onOpenList={action('onOpenList')}
        onClose={action('onClose')}
      />
    </div>
  ))
  .add('Orientation: toLeft', () => (
    <div style={{ position: 'absolute', right: 0, top: 0 }}>
      <ListHeadButtonList
        open={boolean('open', true)}
        lists={lists}
        orientation={ORIENTATION_TO_LEFT}
        onOpen={action('onOpen')}
        onOpenList={action('onOpenList')}
        onClose={action('onClose')}
      />
    </div>
  ));
