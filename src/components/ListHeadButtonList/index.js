import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';

import RoundButton from '../RoundButton';
import { listColors } from '../../utils/colors';
import { DEFAULT_ROUND_SIZE } from '../../constants';

export const ORIENTATION_TO_RIGHT = 'toRight';
export const ORIENTATION_TO_LEFT = 'toLeft';

const styles = () => ({
  container: {
    listStyle: 'none',
    margin: 0,
    padding: 0,
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 6,
  },
  element: {
    position: 'absolute',
    transition: 'all 0.25s ease-in-out',
    zIndex: 5,
  },
});

class ListHeadButtonList extends PureComponent {
  onClickHead = (position, event) => {
    const {
      open, onOpenList, onOpen, lists, onClose,
    } = this.props;
    if (open && onOpenList) {
      if (position === 0) {
        onClose(event);
      } else {
        onOpenList(lists[position - 1].id, event);
      }
    } else if (!open && onOpen) {
      onOpen(event);
    }
  };

  calculateColorBase = position => {
    const { colorMap } = this.props;
    return colorMap[position % colorMap.length];
  };

  calculateColorVar = position => {
    const { colorMap } = this.props;
    const value = Math.ceil(position / colorMap.length);
    return value <= 4 ? value * 100 + 500 : (value - 4) * 100;
  };

  render() {
    const {
      loading, lists, classes, open, size, offset, margin, orientation,
    } = this.props;
    if (loading) {
      return <RoundButton size={size} loading />;
    }
    const orientationProp = orientation === ORIENTATION_TO_RIGHT ? 'left' : 'right';
    const listWithMainButton = [{ id: 'main' }].concat(lists);
    const listContent = listWithMainButton.map(({ image, id }, position) => {
      const style = {};
      style[orientationProp] = open && position > 0 ? offset + position * (size + margin) : 0;
      let buttonProps = {};
      if (id !== 'main') {
        buttonProps = image
          ? { img: image }
          : {
            colorBase: this.calculateColorBase(position),
            colorVar: this.calculateColorVar(position),
          };
      } else {
        buttonProps.icon = <CloseIcon />;
      }
      return (
        <li key={`ListHeadButton-${id}`} className={classes.element} style={style}>
          <RoundButton size={size} {...buttonProps} onClick={e => this.onClickHead(position, e)} />
        </li>
      );
    });

    return <ul className={classes.container}>{listContent}</ul>;
  }
}

ListHeadButtonList.propTypes = {
  // eslint-disable-next-line
  classes: PropTypes.object.isRequired,
  colorMap: PropTypes.arrayOf(PropTypes.object),
  lists: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      image: PropTypes.string,
    }),
  ).isRequired,
  loading: PropTypes.bool,
  margin: PropTypes.number,
  offset: PropTypes.number,
  onClose: PropTypes.func,
  onOpen: PropTypes.func,
  onOpenList: PropTypes.func,
  open: PropTypes.bool,
  orientation: PropTypes.string,
  size: PropTypes.number,
};

ListHeadButtonList.defaultProps = {
  colorMap: listColors,
  loading: false,
  margin: 10,
  offset: 0,
  onClose: () => {},
  onOpen: () => {},
  onOpenList: () => {},
  open: false,
  orientation: ORIENTATION_TO_RIGHT,
  size: DEFAULT_ROUND_SIZE,
};

export default withStyles(styles)(ListHeadButtonList);
