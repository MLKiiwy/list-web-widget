import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';

import RoundButton from '..';

import imageFile from '../../../../static/images/listHead_test.jpg';

storiesOf('RoundButton', module)
  .add('Default', () => <RoundButton />)
  .add('Default loading', () => <RoundButton loading />)
  .add('Set color base green', () => <RoundButton colorBase={green} />)
  .add('Set color base green loading', () => <RoundButton colorBase={green} loading />)
  .add('With image', () => <RoundButton img={imageFile} />)
  .add('With image loading', () => <RoundButton img={imageFile} loading />)
  .add('On click test', () => <RoundButton onClick={action('click')} />)
  .add('100x100', () => <RoundButton size={100} />)
  .add('100x100 with image', () => <RoundButton size={100} img={imageFile} />)
  .add('100x100 with image loading', () => <RoundButton size={100} img={imageFile} loading />)
  .add('Change Icon', () => <RoundButton icon={<CloseIcon />} />);
