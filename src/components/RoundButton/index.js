import React from 'react';
import PropTypes from 'prop-types';
import HashmapPropType from 'hashmap-prop-type';
import Fab from '@material-ui/core/Fab';
import CircularProgress from '@material-ui/core/CircularProgress';
import Avatar from '@material-ui/core/Avatar';
import ListIcon from '@material-ui/icons/List';
import blue from '@material-ui/core/colors/blue';
import { withStyles } from '@material-ui/core/styles';

import { DEFAULT_ROUND_SIZE } from '../../constants';

const styles = () => ({
  wrapper: {
    position: 'relative',
    margin: 0,
    padding: 0,
  },
  fabProgress: {
    position: 'absolute',
    zIndex: 1,
    top: -2,
    left: -2,
  },
  buttonProgress: {
    position: 'absolute',
    width: '100%',
    height: '100%',
  },
  avatar: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    pointerEvents: 'none',
  },
});

function ListHeadButton(props) {
  const {
    img, colorBase, colorVar, onClick, size, classes, loading, icon, id,
  } = props;
  const wrapperProperties = {
    style: {
      width: size,
      height: size,
    },
    className: classes.wrapper,
  };
  const buttonProperties = {
    className: classes.buttonProgress,
    onClick,
  };
  if (img) {
    buttonProperties.src = img;
    buttonProperties.style = { backgroundColor: 'transparent' };
  } else {
    buttonProperties.style = { backgroundColor: colorBase[colorVar] };
  }
  const content = img ? <Avatar className={classes.avatar} src={img} /> : React.cloneElement(icon, { style: { width: '60%', height: '60%' } });
  const progressProperties = {
    className: classes.fabProgress,
    size: size + 4,
    style: {
      color: colorBase[(colorVar + 200) % 900],
    },
    thickness: 2,
  };
  if (id) {
    buttonProperties.id = id;
  }
  const loader = loading ? <CircularProgress {...progressProperties} /> : null;
  return (
    <div {...wrapperProperties}>
      <Fab {...buttonProperties}>
        {content}
      </Fab>
      {loader}
    </div>
  );
}

ListHeadButton.propTypes = {
  // eslint-disable-next-line
  classes: PropTypes.object.isRequired,
  colorBase: HashmapPropType(PropTypes.string.isRequired),
  colorVar: PropTypes.number,
  icon: PropTypes.element,
  id: PropTypes.string,
  img: PropTypes.string,
  loading: PropTypes.bool,
  onClick: PropTypes.func,
  size: PropTypes.number,
};

ListHeadButton.defaultProps = {
  colorBase: blue,
  colorVar: 500,
  icon: <ListIcon />,
  id: undefined,
  img: undefined,
  loading: false,
  onClick: undefined,
  size: DEFAULT_ROUND_SIZE,
};

export default withStyles(styles)(ListHeadButton);
