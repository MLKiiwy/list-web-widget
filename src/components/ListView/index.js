import React from 'react';
import PropTypes from 'prop-types';
import List from '@material-ui/core/List';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import ListItem from '../ListItem';
import ErrorLabel from '../ErrorLabel';

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
  },
  loader: {
    margin: theme.spacing.unit * 2,
  },
  errorLabel: {
    margin: theme.spacing.unit * 2,
  },
  list: {
    flexGrow: 1,
  },
});

function ListView({ children, loading, error, classes, className, items }) {
  const classNameValue = clsx(classes.root, className);

  function renderListItem(item) {
    return React.cloneElement(children, { item, key: item.id });
  }

  return (
    <div className={classNameValue}>
      {loading && <CircularProgress className={classes.loader} disableShrink />}
      {!loading && error && (
        <ErrorLabel className={classes.errorLabel} onRetry={error.onRetry}>
          {error.message}
        </ErrorLabel>
      )}
      {!loading && !error && (
        <List className={classes.list}>{items.map(item => renderListItem(item))}</List>
      )}
    </div>
  );
}

ListView.propTypes = {
  children: PropTypes.node,
  // eslint-disable-next-line
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  error: PropTypes.shape({
    message: PropTypes.string.isRequired,
    onRetry: PropTypes.func,
  }),
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  ),
  loading: PropTypes.bool,
};

ListView.defaultProps = {
  children: <ListItem />,
  className: undefined,
  error: undefined,
  items: [],
  loading: false,
};

export default withStyles(styles)(ListView);
