import React from 'react';
import { storiesOf } from '@storybook/react';
import Paper from '@material-ui/core/Paper/Paper';

import ListView from '..';

const items = [{ id: '1' }, { id: '2' }, { id: '3' }, { id: '4' }];

storiesOf('ListView', module)
  .addDecorator(story => <Paper>{story()}</Paper>)
  .add('Default', () => <ListView items={items} />)
  .add('Loading', () => <ListView loading />)
  .add('Error', () => <ListView error={{ message: 'An error' }} />);
