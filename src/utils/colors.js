import blue from '@material-ui/core/colors/blue';
import green from '@material-ui/core/colors/green';
import red from '@material-ui/core/colors/red';
import lime from '@material-ui/core/colors/lime';
import purple from '@material-ui/core/colors/purple';
import orange from '@material-ui/core/colors/orange';
import yellow from '@material-ui/core/colors/yellow';

export const listColors = [blue, green, red, lime, orange, purple, yellow];
