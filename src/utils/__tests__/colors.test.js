import { listColors } from '../colors';

describe('colors', () => {
  it('returns an array of colors', () => {
    expect(listColors).toBeInstanceOf(Array);
  });
});
