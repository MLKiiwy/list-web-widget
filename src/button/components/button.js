import { extractUrlFromDomElement } from '../util/index';
import ListEvent from '../events';

export default class ListButton {
  onClickHandler = domElement => {
    const event = new CustomEvent(ListEvent.EVENT_ADD_ITEM, {
      url: extractUrlFromDomElement(domElement),
    });
    window.dispatchEvent(event);

    return false;
  };
}
