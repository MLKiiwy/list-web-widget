/* eslint-disable no-console, class-methods-use-this */
export default class ListWidget {
  constructor() {
    this.loaded = false;
    this.addedToDom = false;
    this.config = {
      source: '',
    };
  }

  setConfig = values => {
    this.config = values;
  };

  addToDom = () => {
    if (this.addedToDom) {
      return true;
    }

    const scriptTag = document.createElement('script');
    scriptTag.setAttribute('src', this.config.source);
    document.body.appendChild(scriptTag);

    this.addedToDom = true;
    return true;
  };

  load = () => {
    if (this.loaded) {
      return true;
    }
    this.addToDom();
  };

  onAddItem(event) {
    console.log('add item in widget');
    console.log(event.detail);
  }
}
