import ListEvents from './events';
import ListButton from './components/button';
import ListWidget from './components/widget';
import ListApi from './api';

export default class ListBundle {
  constructor() {
    this.initialized = false;
    this.api = new ListApi();
    this.widget = new ListWidget();
    this.button = new ListButton();

    this.widgetFilename = 'widgetFileNameNotSet';

    this.initEventListeners();
  }

  init = () => {
    if (this.initialized) {
      return false;
    }

    this.loadConfig();

    this.initialized = true;

    return true;
  };

  loadConfig = () => {
    // eslint-disable-next-line no-console
    console.log('init => Load configuration from the page');

    this.widget.setConfig({
      source: this.widgetFilename,
    });
  };

  initEventListeners = () => {
    window.addEventListener(ListEvents.EVENT_ADD_ITEM, this.widget.onAddItem);
    window.addEventListener(ListEvents.EVENT_ADD_ITEM, this.api.onAddItem);

    if (['complete', 'loaded', 'interactive'].includes(document.readyState) && document.body) {
      this.init();
    } else {
      document.addEventListener('DOMContentLoaded', this.init, false);
    }
  };

  onClickHandler = domElement => {
    this.button.onClickHandler(domElement);
  };

  loadWidget = () => this.widget.load()
}
