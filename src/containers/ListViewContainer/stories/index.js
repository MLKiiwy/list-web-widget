import React from 'react';
import { storiesOf } from '@storybook/react';
import apolloStorybookDecorator from 'apollo-storybook-react';
import { typeDefs } from 'list-graphql';
import Paper from '@material-ui/core/Paper/Paper';

import ListViewContainer from '..';

const mocks = {
  Query: () => ({
    list: () => ({
      id: 'list-1',
      items: [
        { id: 'item-1', name: 'Item 1', description: 'Item description' },
        { id: 'item-2', name: 'Item 2', description: 'Item description' },
      ],
    }),
  }),
};

storiesOf('Containers/ListViewContainer', module)
  .addDecorator(
    apolloStorybookDecorator({
      typeDefs,
      mocks,
    }),
  )
  .addDecorator(story => <Paper>{story()}</Paper>)
  .add('List with 2 items', () => <ListViewContainer open id="list-1" />);
