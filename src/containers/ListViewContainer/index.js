import React from 'react';
import PropTypes from 'prop-types';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import ListView from '../../components/ListView';

const GET_LIST = gql`
  query List($id: String!) {
    list(id: $id) {
      id,
      items {
        id,
        name,
        description
      }
    }
  }
`;

function ListViewContainer({ id, ...props }) {
  return (
    <Query query={GET_LIST} variables={{ id }}>
      {({ loading, error, data }) => {
        if (error) {
          console.info(error);
          return null;
        }
        return <ListView loading={loading} items={data.list ? data.list.items : []} {...props} />;
      }}
    </Query>
  );
}

ListViewContainer.propTypes = {
  id: PropTypes.string.isRequired,
};

export default ListViewContainer;
