import React from 'react';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

import ListHeadButtonList from '../../components/ListHeadButtonList';

const GET_MY_ACTIVE_LISTS = gql`
  query myLists{
    lists {
      id,
      image
    }
  }
`;

function ListHeadButtonListContainer(props) {
  return (
    <Query query={GET_MY_ACTIVE_LISTS}>
      {({ loading, error, data }) => {
        if (error) {
          console.info(error);
          return null;
        }
        return <ListHeadButtonList loading={loading} lists={data.lists || []} {...props} />;
      }}
    </Query>
  );
}

export default ListHeadButtonListContainer;
