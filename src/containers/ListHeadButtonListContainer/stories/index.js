import React from 'react';
import { storiesOf } from '@storybook/react';
import apolloStorybookDecorator from 'apollo-storybook-react';
import { typeDefs } from 'list-graphql';

import imageFile from '../../../../static/images/listHead_test.jpg';

import ListHeadButtonListContainer from '..';

const mocks = {
  Query: () => ({
    lists: () => [{ id: '1', image: '' }, { id: '2', image: imageFile }],
  }),
};

storiesOf('Containers/ListHeadButtonListContainer', module)
  .addDecorator(
    apolloStorybookDecorator({
      typeDefs,
      mocks,
    }),
  )
  .add('with 2 lists', () => <ListHeadButtonListContainer open />);
