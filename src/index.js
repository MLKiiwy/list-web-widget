
import '@babel/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from 'react-apollo';

import ListWidget from './components/ListWidget';
import createApolloClient from './graphql/util/create-apollo-client';

function run() {
  let domAppElement = document.getElementById('list-app');
  if (!domAppElement) {
    domAppElement = document.createElement('list-app');
    document.body.appendChild(domAppElement);
  }

  const client = createApolloClient();

  /* eslint react/jsx-filename-extension: off */
  ReactDOM.render(<ApolloProvider client={client}><ListWidget /></ApolloProvider>, domAppElement);
}

if (['complete', 'loaded', 'interactive'].includes(document.readyState) && document.body) {
  run();
} else {
  document.addEventListener('DOMContentLoaded', run, false);
}
