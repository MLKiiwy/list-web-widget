import { graphql } from 'react-apollo';
import gql from 'graphql-tag';

export const withRoundedButtonListQuery = graphql(gql`
query RoundedButtonListQuery {
  lists {
    id
    name
  }
}
`);
