import ApolloClient from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { SchemaLink } from 'apollo-link-schema';
import { makeExecutableSchema } from 'graphql-tools';
import { resolvers, typeDefs } from 'list-graphql';

const schema = makeExecutableSchema({ typeDefs, resolvers });

// eslint-disable-next-line no-underscore-dangle
const apolloCache = new InMemoryCache(window.__APOLLO_STATE__);

export default options => new ApolloClient({
  cache: apolloCache,
  link: new SchemaLink({ schema }),
  ...options,
});
