module.exports = [
  {
    test: /\.jsx?$/,
    include: /src/,
    loader: 'babel-loader',
    options: {
      sourceMap: true,
    },
  },
  {
    test: /\.jpg/,
    exclude: /node_modules/,
    loader: 'url-loader?limit=10000&mimetype=image/jpg',
  },
];
