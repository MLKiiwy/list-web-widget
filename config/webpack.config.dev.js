const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

const rules = require('./webpack.rules');
const entries = require('./webpack.entries');
const htmlConfig = require('./webpack.html');

module.exports = {
  mode: 'development',
  entry: entries,
  devtool: 'inline-source-map',
  devServer: {
    contentBase: '../dist',
    clientLogLevel: 'none',
    hot: true,
  },
  plugins: [
    new CleanWebpackPlugin(['../dist']),
    new HtmlWebpackPlugin(htmlConfig),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules,
  },
  node: {
    fs: 'empty',
  },
};
