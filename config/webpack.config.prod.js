const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpack = require('webpack');

const rules = require('./webpack.rules');
const entries = require('./webpack.entries');
const htmlConfig = require('./webpack.html');

const BUILD_PATH = path.resolve(__dirname, '../dist');

module.exports = {
  mode: 'production',
  entry: entries,
  devtool: 'source-map',
  plugins: [
    new CleanWebpackPlugin([BUILD_PATH]),
    new HtmlWebpackPlugin(htmlConfig),
    new webpack.NamedModulesPlugin(),
  ],
  output: {
    filename: '[name].[contenthash:8].js',
    path: BUILD_PATH,
  },
  module: {
    rules,
  },
  node: {
    fs: 'empty',
  },
};
