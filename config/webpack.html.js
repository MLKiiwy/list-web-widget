const { generateButtons } = require('../tools/html/buttons');

module.exports = {
  templateParameters: (compilation, assets) => ({
    compilation,
    assets,
    elements: generateButtons(),
  }),
  template: './src/index.html',
  inject: false,
};
