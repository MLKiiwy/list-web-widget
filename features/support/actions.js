const assert = require('assert');

const scope = require('./scope');
const pages = require('./pages');

// Defines whether puppeteer runs Chrome in headless mode.
let headless = true;
let slowMo = 5;
// Chrome is set to run headlessly and with no slowdown in CircleCI
if (process.env.CIRCLECI) headless = true;
if (process.env.CIRCLECI) slowMo = 0;

const pending = callback => {
  callback(null, 'pending');
};

const visitHomepage = async () => {
  if (!scope.browser) {
    scope.browser = await scope.driver.launch({
      headless,
      slowMo,
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
    });
  }
  scope.context.currentPage = await scope.browser.newPage();
  scope.context.currentPage.setViewport({ width: 1280, height: 1024 });
  const url = scope.host;

  const visit = await scope.context.currentPage.goto(url, {
    waitUntil: 'networkidle2',
  });
  return visit;
};

const shouldBeOnPage = async name => {
  assert.equal(await scope.context.currentPage.title(), pages[name].title);
};

module.exports = {
  pending,
  visitHomepage,
  shouldBeOnPage,
};
