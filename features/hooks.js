const { After, Before, AfterAll } = require('cucumber');

const scope = require('./support/scope');

Before(async () => {
  // You can clean up database models here
});

After(async () => {
  // Here we check if a scenario has instantiated a browser and a current page
  if (scope.browser && scope.context.currentPage) {
    // if it has, find all the cookies, and delete them
    const cookies = await scope.context.currentPage.cookies();
    if (cookies && cookies.length > 0) {
      await scope.context.currentPage.deleteCookie(...cookies);
    }
    // close the web page down
    await scope.context.currentPage.close();
    // wipe the context's currentPage value
    scope.context.currentPage = null;
  }
});

AfterAll(async () => {
  if (scope.browser) await scope.browser.close();
  // eslint-disable-next-line no-console
  scope.server.shutdown(() => console.log('Server is shut down'));
});
