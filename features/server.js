/* eslint no-console: ["error", { allow: ["log"] }] */

// Dependencies
//
const express = require('express');
const httpShutdown = require('http-shutdown');
const path = require('path');

const app = express();

const PORT = 5555;

const buildPath = path.resolve(__dirname, '../build');

app.use(express.static(buildPath));

app.get('*', (req, res) => {
  res.sendFile(path.join(buildPath, 'index.html'));
});

// Listen on the port
const server = httpShutdown(
  app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
  }),
);

server.host = `http://localhost:${PORT}`;

module.exports = server;
