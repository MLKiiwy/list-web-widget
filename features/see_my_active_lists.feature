Feature: Load my last active lists
  When widget load, user's active lists are loaded with it.
  So user can continue adding/removing item to its last used list.
  The last used list should be displayed first.
  That mean when widget is collapsed, you should still see the last used list.

  Scenario: Widget load user's active lists on load
    Given I am on the test environment
    And I am a authenticated user
    And my last used list is:
      | title        |
      | lastUsedList |
    And my active lists are:
      | title        |
      | lastUsedList |
      | list1        |
      | list2        |
    When I load the widget
    Then I should see the widget loading
    When I wait for the widget to finish loading
    Then the widget is visible
    And the widget is collapsed
    And I should see the active list "lastUsedList"
    When I click on the main widget button
    Then I should see the active lists:
      | title        |
      | lastUsedList |
      | list1        |
      | list2        |
