const { setWorldConstructor } = require('cucumber');
const puppeteer = require('puppeteer');

const scope = require('./support/scope');
const server = require('./server');

const World = function () {
  scope.host = server.host;
  scope.driver = puppeteer;
  scope.context = {};
  scope.server = server;
};

setWorldConstructor(World);
