const { Given, When, Then } = require('cucumber');

const { visitHomepage, shouldBeOnPage } = require('../support/actions');

Given('I am on the test environment', { timeout: 10000 }, async function () {
  await visitHomepage();
  return shouldBeOnPage('home');
});

Given('I am a authenticated user', async function () {
  return true;
});

Given('my last used list is:', function () {
  return true;
});

Given('my active lists are:', function () {
  return true;
});

When('I load the widget', function () {
  return true;
});

Then('I should see the widget loading', function () {
  return true;
});

When('I wait for the widget to finish loading', function () {
  return true;
});

Then('the widget is visible', function () {
  return true;
});

Then('the widget is collapsed', function () {
  return true;
});

Then('I should see the active list {string}', function () {
  return true;
});

When('I click on the main widget button', function () {
  return true;
});

Then('I should see the active lists:', function () {
  return true;
});
