import faker from 'faker';

import headImage from '../../static/images/listHead_test.jpg';

export default {
  List: () => ({
    name: faker.name.findName(),
    image: headImage,
  }),
};
