import uuid from 'uuid';
import _ from 'lodash';

import headImage from '../../../static/images/listHead_test.jpg';

export const listItem = () => ({
  id: uuid(),
  name: null,
  image: null,
  description: '',
  lastModifier: null,
  creator: null,
  position: 0,
  createdAt: null,
  updatedAt: null,
  item: {
    type: 'http://schema.org/Thing',
    name: '',
    image: null,
    description: '',
    url: null,
    data: {},
  },
});

export const list = (itemsNumber = 4, withImage = false) => ({
  id: uuid(),
  name: 'unamed list',
  image: withImage ? headImage : null,
  description: '',
  numberOfItems: 2,
  itemListOrder: 'ItemListUnordered',
  lastModifier: null,
  creator: null,
  createdAt: null,
  updatedAt: null,
  items: _.range(itemsNumber).map(listItem),
});

export const lists = (listNumber = 4) => _.range(listNumber).map(i => list(i + 1, (i % 2) === 0));
