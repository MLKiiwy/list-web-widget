const _ = require('lodash');

const urls = require('../mocks/urls/urls.json');

const generateButtons = (nb = 15, tag = 'a', container = 'ul') => {
  const buttons = [];
  const keys = _.keys(urls);
  for (let i = 0; i < nb && i < keys.length; i += 1) {
    const name = keys[i];
    const url = urls[name];
    if (tag === 'a') {
      buttons.push(`<a onClick="window.ListBundle.onClickHandler(this);" href="${url}">${name}</a>`);
    }
  }

  if (container === 'ul') {
    return `<ul>${_.join(buttons.map(e => `<li>${e}</li>`), '')}</ul>`;
  }
  return _.join(buttons, '');
};

module.exports = {
  generateButtons,
};
